

## Getting started

- Install `npm` dependencies vis `npm install`
- Launch ngrok via `./ngrok http 8080`
- Launch the node-server via forever, as it should be restarted on filechanges: `forever -w cookbook.js`
- From the ASK-developer console set the endpoint of the skill to:
    `<ngrok-https-address>/mein-kochbuch` __(or the actual app-name)__
    
Visit <http://localhost:8080/mein-kochbuch>, which will provide the current JSON build-model tp paste in the __Alexa Developer Console__.