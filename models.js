let _ = require('lodash');
let assert = require('assert');
let CONSTANTS = require('./constants');

class Recipe{
    constructor (name, category, ingredients, instructions, ingredientIndex, instructionsIndex) {
        assert(typeof name === 'string', 'Name is no string');
        assert(_.isArrayLike(instructions), 'instructions are no array.');
        assert(instructions.length > 0, 'instructions are empty.');

        assert(typeof category === 'string', 'Category is no string');
        assert(CONSTANTS.CATEGORY_SLOTS_VALUES_ARRAY.includes(category), 'Provided category not found in the list of available categories: ' + category);

        assert(_.isArrayLike(ingredients), 'ingredients are no array.');
        assert(ingredients.length > 0, 'ingredients are empty.');

        ingredientIndex = ingredientIndex || 0;
        instructionsIndex = instructionsIndex || 0;
        assert((ingredientIndex >= 0) && (ingredientIndex <= ingredients.length - 1), 'Ingredient index out of bounds: ' + ingredientIndex);
        assert((instructionsIndex >= 0) && (instructionsIndex <= instructions.length -1), 'Instruction index out of bounds: ' + instructionsIndex);
        this.name = name;

        this.category = category;

        this.ingredients = ingredients;
        this.instructions = instructions;

        this.ingredientIndex =  ingredientIndex;
        this.instructionIndex = instructionsIndex;
    }

    getName() {
        return this.name;
    }
    getCategory() {
        return this.category;
    }
    getIngredients() {
        return this.ingredients;
    }
    getInstructions() {
        return this.instructions;
    }

    toJSONString() {
        return '{' +
            '"name":"' + this.name + '",' +
            '"category":"' + this.category + '",' +
            '"ingredientIndex":' + this.ingredientIndex + ',' +
            '"instructionIndex":' + this.instructionIndex + ',' +
            '"instructions":[' + _.map(this.instructions, (instruction) => '"' + instruction + '"') + '],' +
            '"ingredients":[' + _.map(this.ingredients, (ingredient) => '"' + ingredient + '"') + ']' +
        '}';
    }

    toJSON() {
        return JSON.parse(this.toJSONString());
    }

    hasNextIngredient() {
        return !(this.ingredientIndex === (this.ingredients.length - 1));
    }
    hasPreviousIngredient() {
        return !(this.ingredientIndex === 0);
    }
    nextIngredient() {
        this.ingredientIndex = Math.min((this.ingredients.length - 1), (this.ingredientIndex + 1));
    }
    previousIngredient() {
        this.ingredientIndex = Math.max(0, (this.ingredientIndex - 1));
    }
    resetIngredients() {
        this.ingredientIndex = 0;
    }
    getCurrentIngredient() {
        return this.ingredients[this.ingredientIndex];
    }
    lastIngredient() {
        this.ingredientIndex = this.ingredients.length - 1;
    }

    hasNextInstruction() {
        return !(this.instructionIndex === (this.instructions.length - 1));
    }
    hasPreviousInstruction() {
        return !(this.instructionIndex === 0);
    }
    nextInstruction() {
        this.instructionIndex = Math.min((this.instructions.length - 1), (this.instructionIndex + 1));
    }
    previousInstruction() {
        this.instructionIndex = Math.max(0, (this.instructionIndex - 1));
    }
    resetInstructions() {
        this.instructionIndex = 0;
    }
    getCurrentInstruction() {
        return this.instructions[this.instructionIndex];
    }
}

function runTest() {
    let recipe = new Recipe('Beispielrezept', 'starter', ['Zutat 1', 'Zutat 2', 'Zutat 3'], ['Schritt 1', 'Schritt 2', 'Schritt 3'], 1);
    console.log(recipe.getName());
    console.log(recipe.getCategory());
    console.log(recipe.getIngredients());
    console.log(recipe.getInstructions());
    console.log('#########################');
    console.log(recipe.getCurrentIngredient());
    console.log(recipe.hasNextIngredient());
    recipe.nextIngredient();
    console.log(recipe.getCurrentIngredient());
    recipe.nextIngredient();
    console.log(recipe.getCurrentIngredient());
    console.log(recipe.hasNextIngredient());
    recipe.nextIngredient();
    console.log(recipe.getCurrentIngredient());
    recipe.resetIngredients();
    console.log(recipe.getCurrentIngredient());
    console.log('########################');
    console.log(recipe.getCurrentInstruction());
    console.log(recipe.hasNextInstruction());
    recipe.nextInstruction();
    console.log(recipe.getCurrentInstruction());
    recipe.nextInstruction();
    console.log(recipe.getCurrentInstruction());
    console.log(recipe.hasNextInstruction());
    recipe.nextInstruction();
    console.log(recipe.getCurrentInstruction());
    recipe.resetInstructions();
    console.log(recipe.getCurrentInstruction());
    console.log('######################');
    console.log(recipe.toJSONString());
}

// runTest();

module.exports = {Recipe};
