/**
 * A cookbook ALEXA skill
 *
 */
let express = require("express");
let alexa = require("alexa-app");
let AmazonSpeech = require('ssml-builder/amazon_speech');
let Handler = require("./handler");
let Helpers = require("./helpers");
let CONSTANTS = require('./constants');

let PORT = process.env.PORT || 8080;

let app = express();
let alexaApp = new alexa.app("mein-kochbuch");
alexaApp.invocationName = 'mein kochbuch';

alexaApp.express({
    expressApp: app,
    checkCert: false,
    debug: true
});

app.set("view engine", "ejs");

alexaApp.launch(function(request, response) {

    // TODO: Remove!!!!!!!!!!!!1!!!!!!!!!!!
    /*let speech = new AmazonSpeech().say('Meine Rezepte');
    Helpers.setAppState(request, CONSTANTS.APP_STATES.SEARCH_MY_RECIPIES);
    response.say(speech.ssml(true)).shouldEndSession(false);*/

    Helpers.setAppState(request, CONSTANTS.APP_STATES.START);

    let now = new Date();
    let greetingSpeech = Helpers.getGreetingSpeech(now);

    response
        .say(greetingSpeech.ssml(true))
        .shouldEndSession(false)
});

/*
 * Slot definitions
 */
alexaApp.customSlot('CategoryTypes', CONSTANTS.CATEGORY_SLOTS);

/*
 * Decission intents
 */
alexaApp.intent("yes_intent", {
        "slots": {},
        "utterances": [
            '{ja|jo} {gerne|mag ich|}',
        ]
    },
    Handler.handleYesIntent
);
alexaApp.intent("no_intent", {
        "slots": {},
        "utterances": [
            '{nein|nö|auf keinen fall|nein mag ich nicht}'
        ]
    },
    Handler.handleNoIntent
);

/*
 * Navigation intents
 */
alexaApp.intent("next_intent", {
        "slots": {},
        "utterances": [
            '{gehe|und|} {eine zutat|einen schritt|} weiter',
        ]
    },
    Handler.handleNextIntent
);
alexaApp.intent("previous_intent", {
        "slots": {},
        "utterances": [
            '{gehe|und|} {eine zutat|einen schritt|} zurück',
        ]
    },
    Handler.handlePreviousIntent
);
alexaApp.intent("repeat_single_intent", {
        "slots": {},
        "utterances": [
            'wiederhole {die zutat|den schritt}'
        ]
    },
    Handler.handleRepeatSingleIntent
);
alexaApp.intent("repeat_all_intent", {
        "slots": {},
        "utterances": [
            'wiederhole alle {zutaten|schritte}'
        ]
    },
    Handler.handleRepeatAllIntent
);

/*
 * Type intents
 */
alexaApp.intent("invoke_name_intent", {
        "slots": {},
        "utterances": [
            'nach {name|namen} suchen'
        ]
    },
    Handler.handleInvokeNameIntent
);
alexaApp.intent("invoke_category_intent", {
        "slots": {},
        "utterances": [
            'nach {kategorie|kategorien} suchen'
        ]
    },
    Handler.handleInvokeCategoryIntent
);
alexaApp.intent("invoke_ingredient_intent", {
        "slots": {},
        "utterances": [
            'nach {zutat|zutaten} suchen'
        ]
    },
    Handler.handleInvokeIngredientIntent
);
alexaApp.intent("invoke_my_recipes_intent", {
        "slots": {},
        "utterances": [
            'nach meinen rezepten suchen'
        ]
    },
    Handler.handleInvokeMyRecipesIntent
);

/*
 * Retrieve data intents
 */
alexaApp.intent("search_by_category_intent", {
        "slots": {'CATEGORY': 'CategoryTypes'},
        "utterances": [
            'durchsuche die kategorie {-|CATEGORY}'
        ]
    },
    Handler.handleSearchByCategoryIntent
);
alexaApp.intent("search_by_name_intent", {
        "slots": {"NAME": "AMAZON.SearchQuery"},
        "utterances": [
            'durchsuche alle rezepte nach {-|NAME}'
        ]
    },
    Handler.handleSearchByNameIntent
);
alexaApp.intent("search_by_ingredient_intent", {
        "slots": {"INGREDIENT": "AMAZON.SearchQuery"},
        "utterances": [
            'durchsuche die rezepte nach {den zutaten|der zutat} {-|INGREDIENT}'
        ]
    },
    Handler.handleSearchByIngredientIntent
);
alexaApp.intent("search_my_recipes_by_name_intent", {
        "slots": {"NAME": "AMAZON.SearchQuery"},
        "utterances": [
            'durchsuche meine rezepte nach {-|NAME}'
        ]
    },
    Handler.handleSearchMyRecipesByNameIntent
);

/*
 * Built-in intents
 */

alexaApp.intent("AMAZON.HelpIntent", {
        "slots": {},
        "utterances": [
            "kochbuch {hilfe|hilf mir}"
        ]
    },
    Handler.handleHelpIntent
);

alexaApp.intent("AMAZON.StopIntent", {
        "slots": {},
        "utterances": [
            'kochbuch {schließen|beenden}'
        ]
    },
    Handler.handleStopIntent
);

alexaApp.intent("AMAZON.CancelIntent", {
        "slots": {},
        "utterances": [
            'kochbuch {rezept|schritt|} abbrechen'
        ]
    },
    Handler.handleCancelIntent
);

alexaApp.error = Handler.handleError;
alexaApp.sessionEnded(Handler.handleSessionEnded);

app.listen(PORT);
console.log("Listening on port " + PORT + ".");