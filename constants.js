let _ = require('lodash');

/*
 * Slot definitions
 */
const CATEGORY_SLOTS = [
    {
        id: 'SEAFOOD',
        value: 'seafood',
        synonyms: ['fisch', 'meeresfrüchte']
    },{
        id: 'VEGETARIAN',
        value: 'vegetarian',
        synonyms: ['vegetarisches', 'vegetarisch']
    },{
        id: 'VEGAN',
        value: 'vegan',
        synonyms: ['veganes']
    },{
        id: 'STARTER',
        value: 'starter',
        synonyms: ['vorspeisen', 'vorspeise']
    },{
        id: 'SIDE',
        value: 'side',
        synonyms: ['beilagen', 'beilage']
    },{
        id: 'PORK',
        value: 'pork',
        synonyms: ['schwein']
    },{
        id: 'LAMB',
        value: 'lamb',
        synonyms: ['lamm']
    },{
        id: 'PASTA',
        value: 'pasta',
        synonyms: ['nudeln', 'nudelgerichte', 'nudelgericht']
    },{
        id: 'DESERT',
        value: 'desert',
        synonyms: ['desserts', 'dessert', 'nachtisch', 'nachspeise']
    },{
        id: 'CHICKEN',
        value: 'chicken',
        synonyms: ['huhn', 'hühnchen']
    },{
        id: 'BEEF',
        value: 'beef',
        synonyms: ['rind']
    },{
        id: 'MISCELLANEOUS',
        value: 'miscellaneous',
        synonyms: ['beliebig']
    }
];

const AVAILABLE_CATEGORIES_ARRAY = _.map(CATEGORY_SLOTS, (slotObject) => slotObject.synonyms[0]);

const CATEGORY_SLOTS_VALUES_ARRAY = _.map(CATEGORY_SLOTS, (slotObject) => slotObject.value);

/*
 * States
 */
const APP_STATE = 'APP_STATE';
const APP_STATES = {
    START: 'START',
    SEARCH_RANDOM: 'SEARCH_RANDOM',
    SEARCH_NAME: 'SEARCH_NAME',
    SEARCH_CATEGORY: 'SEARCH_CATEGORY',
    SEARCH_INGREDIENT: 'SEARCH_INGREDIENT',
    SEARCH_MY_RECIPIES: 'SEARCH_MY_RECIPIES',
    MENU: 'MENU',
    RESULT: 'RESULT',
    END: 'END'
};
const RESULT_STATE = 'RESULT_STATE';
const RESULT_STATES = {
    IN_INGREDIENTS: 'IN_INGREDIENTS',
    IN_INSTRUCTIONS: 'IN_INSTRUCTIONS',
};

const RECIPE = 'RECIPE';

const SEARCH_STRING = 'SEARCH_STRING';

module.exports = {CATEGORY_SLOTS, AVAILABLE_CATEGORIES_ARRAY, CATEGORY_SLOTS_VALUES_ARRAY, APP_STATE, APP_STATES, RECIPE, RESULT_STATE, RESULT_STATES, SEARCH_STRING};