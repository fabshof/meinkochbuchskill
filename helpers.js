let _ = require('lodash');
let assert = require('assert');
let AmazonSpeech = require('ssml-builder/amazon_speech');

let Models = require('./models');
let API = require('./recipe-api');
let CONSTANTS = require('./constants');

/*
 * Helper functions
 */
let getAlternateRecipeString = function (recipeName) {

    let alternateResponses = [
        'Wie sieht es mit ' + recipeName + ' aus?',
        'Magst du ' + recipeName + ' lieber?',
        'Und was ist mit ' + recipeName,
        'Möchtest du lieber ' + recipeName + ' kochen?',
        'Schmeckt dir ' + recipeName + ' besser?'
    ];
    return alternateResponses[Math.floor(alternateResponses.length * Math.random())]
};

let getSlotValueOrNull = function (request, slotName) {
    assert(typeof slotName === 'string', 'slotName is no string');
    assert(request != null, 'request is empty');

    let slot = request.slots[slotName];
    if (!_.isEmpty(slot)) {
        if (slot.resolutions && slot.resolutions.length > 0) {
            let slotResolution = slot.resolution(0);
            if (!_.isEmpty(slotResolution)) {
                return slotResolution.first().name;
            }
        }
        return slot.value;
    }
    return null;
};

let getSessionValueOrNull = function(request, attributeName) {
    assert(typeof attributeName === 'string', 'attributeName is no string');
    assert(request != null, 'request is empty');

    if (request.hasSession()) {
        let session = request.getSession();
        return session.get(attributeName);
    }
    return null;
};
let setSessionValue = function(request, attributeName, attributeValue) {
    assert(typeof attributeName === 'string', 'attributeName is no string');
    assert(typeof attributeValue === 'string', 'attributeValue is no string');
    assert(request != null, 'request is empty');

    if (request.hasSession()) {
        let session = request.getSession();
        session.clear(attributeName);
        session.set(attributeName, attributeValue);
        return true;
    }
    return false;
};
let resetSessionValue = function(request, attributeName) {
    assert(typeof attributeName === 'string', 'attributeName is no string');
    assert(request != null, 'request is empty');

    if (request.hasSession()) {
        let session = request.getSession();
        session.clear(attributeName);
        return true;
    }
    return false;
};

let cannotBeUsedHereResponse = function (response) {
    let speech = new AmazonSpeech()
        .say('Entschuldigung, aber der Befehl kann hier nicht benutzt werden.')
        .say('Sag ').emphasis('moderate', 'kochbuch hilf mir').say('und ich helfe dir weiter');
    response.say(speech.ssml(true)).shouldEndSession(false);
};

/*
 * Implementations
 */
let setAppState = function (request, appState) {
    assert(request != null, 'request is empty');
    assert(Object.values(CONSTANTS.APP_STATES).indexOf(appState) > -1, 'Provided appState not found in the list of available states: ' + appState);
    setSessionValue(request, CONSTANTS.APP_STATE, appState);
};
let getAppState = function (request) {
    return getSessionValueOrNull(request, CONSTANTS.APP_STATE);
};
let resetAppState = function (request) {
    resetSessionValue(request, CONSTANTS.APP_STATE);
};

let setRecipe = function (request, recipe) {
    assert(request != null, 'request is empty');
    assert(recipe != null, 'recipe is empty');
    setSessionValue(request, CONSTANTS.RECIPE, recipe.toJSONString());
};
let getRecipe = function (request) {
    let recipe = getSessionValueOrNull(request, CONSTANTS.RECIPE);
    if (recipe != null) {
        recipe = JSON.parse(recipe);
        return new Models.Recipe(recipe.name, recipe.category, recipe.ingredients, recipe.instructions, recipe.ingredientIndex, recipe.instructionIndex);
    }
    return null;
};
let resetRecipe = function (request) {
    assert(request != null, 'request is empty');
    resetSessionValue(request, CONSTANTS.RECIPE);
};

let setResultState = function (request, resultState) {
    assert(request != null, 'request is empty');
    assert(Object.values(CONSTANTS.RESULT_STATES).indexOf(resultState) > -1, 'Provided resultState not found in the list of available states: ' + resultState);
    setSessionValue(request, CONSTANTS.RESULT_STATE, resultState);
};
let getResultState = function (request) {
    return getSessionValueOrNull(request, CONSTANTS.RESULT_STATE);
};
let resetResultState = function (request) {
    assert(request != null, 'request is empty');
    resetSessionValue(request, CONSTANTS.RESULT_STATE);
};

let setSearchString = function (request, searchString) {
    assert(request != null, 'request is empty');
    assert(typeof searchString === 'string', 'SearchString is not a string: ' + typeof searchString);
    assert(searchString !== '', 'SearchString is empty');
    setSessionValue(request, CONSTANTS.SEARCH_STRING, searchString);
};
let getSearchString = function (request) {
    assert(request != null, 'request is empty');
    return getSessionValueOrNull(request, CONSTANTS.SEARCH_STRING);
};
let resetSearchString = function (request) {
    assert(request != null, 'request is empty');
    resetSessionValue(request, CONSTANTS.SEARCH_STRING);
};

function getGreetingIndex(dateNow) {
    let hour = dateNow.getHours();
    if (hour < 9) {
        return 0; // Morgen
    } else if (hour < 11) {
        return 1; // Vormittag
    } else if (hour < 14) {
        return 2; // Mittag
    } else if (hour < 19) {
        return 3; // Nachmittag
    } else {
        return 4; // Abend
    }
}
let getGreetingFunction = function(dateNow) {

    let month = dateNow.getMonth();
    let greetingIndex = getGreetingIndex(dateNow);

    switch (greetingIndex) {
        case 0: // Morgen
            return API.FoodAPI.getRecipeByName('breakfast');
        case 2: // Mittag
            if ((month >= 6) && (month < 9)) { // was leichtes
                return API.FoodAPI.getRecipeByName('salad');
            } else if (month === 9) { // was mit kuerbis
                return API.FoodAPI.getRecipeByName('pumpkin');
            } else if (month > 9) { // warmes == suppe
                return API.FoodAPI.getRecipeByName('soup');
            } else { // default
                return API.FoodAPI.getRandomRecipe();
            }
        case 3: // Nachmittag
                return API.FoodAPI.getRecipeByName('cake');
        default:
            return API.FoodAPI.getRandomRecipe();
    }
};

let getGreetingSpeech = function (dateNow) {

    let days = ['Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag', 'Sonntag'];
    let intervals = ['Morgen', 'Vormittag', 'Mittag', 'Nachmittag', 'Abend'];

    let speech = new AmazonSpeech();

    let tmp = dateNow.getDay() - 1;
    let day = tmp < 0 ? tmp + 7 : tmp;
    let month = dateNow.getMonth();

    let greetingIndex = getGreetingIndex(dateNow);
    let interval = intervals[greetingIndex];

    speech
        .say('Hi, einen wunderschönen ' + days[day] + ' ' + interval + ' ')
        .pause('200ms');

    switch (greetingIndex){
        case 0: // Morgen
            speech.say('Soll ich dir ein Frühstücks Rezept vorschlagen?');
            break;
        case 2: // Mittag
            if ((month >= 6) && (month < 9)) {
                speech.say('Möchtest du was leichtes zu Mittag essen?');
            } else if (month === 9) {
                speech.say('Magst du was herbstliches mit Kürbis essen?');
            } else if (month > 9) {
                speech.say('Möchtest du was Warmes zu Mittag?');
            } else {
                speech.say('Darf ich dir was zu Mittag vorschlagen?');
            }
            break;
        case 3: // Nachmittag
            speech.say('Möchtest du was süßes ?');
            break;
        default: // Default
            speech.say('Darf ich dir was zu Essen vorschlagen?');
    }
    return speech;
};

function runTest() {

    for (let j = 0; j < 24; j++) {
        for (let i = 0; i < 12; i++) {
            let date = new Date();
            date.setHours(j);
            date.setMonth(i);
            console.log((date.getMonth() + 1) +  ' ' + date.getHours() + ':00' + getGreetingSpeech(date).ssml(true));
        }
    }
}

// runTest();


module.exports = {getAlternateRecipeString, getSlotValueOrNull, setAppState, getAppState, resetAppState, setRecipe, getRecipe,
    resetRecipe, cannotBeUsedHereResponse, getResultState, setResultState, resetResultState, setSearchString, getSearchString,
    resetSearchString, getGreetingFunction, getGreetingSpeech};
