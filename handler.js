require('alexa-app');
let _ = require('lodash');
let AmazonSpeech = require('ssml-builder/amazon_speech');
let Helpers = require('./helpers');
let CONSTANTS = require('./constants');
let API = require('./recipe-api');
let DB = require('./database-api');

/*
 * Decissions
 */
let handleYesIntent = function(request, response) {
    console.log('' + arguments.callee.name);
    // Start => Random recipe
    if (Helpers.getAppState(request) === CONSTANTS.APP_STATES.START) {
        Helpers.setAppState(request, CONSTANTS.APP_STATES.SEARCH_RANDOM);

        let delegate = Helpers.getGreetingFunction(new Date());
        return delegate
            .then((responseRecipe) => {
                Helpers.setRecipe(request, responseRecipe);
                let name = responseRecipe.getName();
                response
                    .say('Okay, möchtest du ' + name + ' zusammen mit mir kochen?')
                    .shouldEndSession(false);
            });
    // search for recipe => yes
    } else if((Helpers.getAppState(request) === CONSTANTS.APP_STATES.SEARCH_RANDOM)
        || (Helpers.getAppState(request) === CONSTANTS.APP_STATES.SEARCH_CATEGORY)
        || (Helpers.getAppState(request) === CONSTANTS.APP_STATES.SEARCH_NAME)
        || (Helpers.getAppState(request) === CONSTANTS.APP_STATES.SEARCH_INGREDIENT)) {

        Helpers.setAppState(request, CONSTANTS.APP_STATES.RESULT);
        Helpers.setResultState(request, CONSTANTS.RESULT_STATES.IN_INGREDIENTS);
        Helpers.resetSearchString(request);

        let recipe = Helpers.getRecipe(request);
        let speech = new AmazonSpeech().say('Okay, hier kommt dein Rezept.')
            .pause('200ms')
            .say('Für ' + recipe.name + ' brauchst du: ' + recipe.getCurrentIngredient());

        Helpers.setRecipe(request, recipe);

        response.say(speech.ssml(true)).shouldEndSession(false);
    // recipe end => should store? => yes
    } else if(Helpers.getAppState(request) === CONSTANTS.APP_STATES.END) {
        let recipe = Helpers.getRecipe(request);
        let dbAPI = new DB.DatabaseAPI();
        recipe.resetIngredients();
        recipe.resetInstructions();
        dbAPI.saveRecipe(recipe);

        let speech = new AmazonSpeech().say('Okay, ich habe es in deinen persönlichen Rezepten gespeichert.').pause('300ms').say('Einen guten Appetit und bis bald');
        response.say(speech.ssml(true)).shouldEndSession(true);
    } else {
        Helpers.cannotBeUsedHereResponse(response);
    }
};
let handleNoIntent = function(request, response) {
    console.log('' + arguments.callee.name);
    // Start => Menu
    if (Helpers.getAppState(request) === CONSTANTS.APP_STATES.START) {
        Helpers.setAppState(request, CONSTANTS.APP_STATES.MENU);
        let speech = new AmazonSpeech()
            .say('Okay, du bist jetzt im Menü. hier kannst du nach')
            .emphasis('moderate', 'Namen, Zutaten, Kategorien oder deinen gespeicherten Rezepten')
            .say('suchen. Nach was würdest du gerne suchen?');
        response.say(speech.ssml(true)).shouldEndSession(false);
    // Dislike random recipe and search a new one
    } else if (Helpers.getAppState(request) === CONSTANTS.APP_STATES.SEARCH_RANDOM
        || Helpers.getAppState(request) === CONSTANTS.APP_STATES.SEARCH_NAME
        || Helpers.getAppState(request) === CONSTANTS.APP_STATES.SEARCH_CATEGORY
        || Helpers.getAppState(request) === CONSTANTS.APP_STATES.SEARCH_INGREDIENT) {

        let delegate;
        // searchRandomRecipe is the only method that doesn't need a searchstring
        if (Helpers.getAppState(request) === CONSTANTS.APP_STATES.SEARCH_RANDOM) {
            delegate = API.FoodAPI.getRandomRecipe();
        } else {
            let searchString = Helpers.getSearchString(request);
            if (Helpers.getAppState(request) === CONSTANTS.APP_STATES.SEARCH_NAME) {
                delegate = API.FoodAPI.getRecipeByName(searchString);
            } else if (Helpers.getAppState(request) === CONSTANTS.APP_STATES.SEARCH_CATEGORY) {
                delegate = API.FoodAPI.getRecipeByCategory(searchString);
            } else if (Helpers.getAppState(request) === CONSTANTS.APP_STATES.SEARCH_INGREDIENT) {
                delegate = API.FoodAPI.getRecipeByIngredient(searchString);
            }
        }
        return delegate
            .then((responseRecipe) => {
                if (responseRecipe != null) {
                    Helpers.setRecipe(request, responseRecipe);
                    let name = responseRecipe.getName();
                    response
                        .say(Helpers.getAlternateRecipeString(name))
                        .reprompt(Helpers.getAlternateRecipeString(name))
                        .shouldEndSession(false);
                } else {
                    response
                        .say('Hm, ich konnte leider kein Rezept finden.')
                        .shouldEndSession(false);
                }
            });
        // recipe end => should store? => yes
    } else if(Helpers.getAppState(request) === CONSTANTS.APP_STATES.END) {
        let speech = new AmazonSpeech().say('Einen guten Appetit und bis bald');
        response.say(speech.ssml(true)).shouldEndSession(true);
    } else {
        Helpers.cannotBeUsedHereResponse(response);
    }
};


let handleNextIntent = function(request, response) {
    console.log('' + arguments.callee.name);
    // Inside of a recipe result
    if (Helpers.getAppState(request) === CONSTANTS.APP_STATES.RESULT) {
        let recipe = Helpers.getRecipe(request);
        let speech = new AmazonSpeech();
        let shouldEndSession = false;

        // Recipe => ingredients
        if (Helpers.getResultState(request) === CONSTANTS.RESULT_STATES.IN_INGREDIENTS) {
            if (!recipe.hasNextIngredient()) {
                Helpers.setResultState(request, CONSTANTS.RESULT_STATES.IN_INSTRUCTIONS);
                recipe.resetInstructions();
                speech.say('Okay, und jetzt die Arbeitsschritte:')
                    .say(recipe.getCurrentInstruction());
            } else {
                recipe.nextIngredient();
                speech.say(recipe.getCurrentIngredient());
            }
        // Recipe => instructions
        } else if (Helpers.getResultState(request) === CONSTANTS.RESULT_STATES.IN_INSTRUCTIONS) {
            if (!recipe.hasNextInstruction()) {

                let dbAPI = new DB.DatabaseAPI();
                let isRecipeStored = dbAPI.findRecipe(recipe.getName());

                speech.say('Dein Gericht ist jetzt fertig.').pause('300ms');
                if (!isRecipeStored) {
                    speech.say('Möchtest du das Rezept in deinen persönlichen Rezepten speichern?');
                    Helpers.setAppState(request, CONSTANTS.APP_STATES.END);
                } else {
                    speech.say('Einen guten Appetit. Bis bald.');
                    shouldEndSession = true;
                }
            } else {
                recipe.nextInstruction();
                speech.say(recipe.getCurrentInstruction());
            }
        }
        Helpers.setRecipe(request, recipe);
        response.say(speech.ssml(true)).shouldEndSession(shouldEndSession);
    } else {
        Helpers.cannotBeUsedHereResponse(response);
    }
};
let handlePreviousIntent = function(request, response) {
    console.log('' + arguments.callee.name);
    if (Helpers.getAppState(request) === CONSTANTS.APP_STATES.RESULT) {
        let recipe = Helpers.getRecipe(request);
        let speech = new AmazonSpeech();

        // Recipe => ingredients
        if (Helpers.getResultState(request) === CONSTANTS.RESULT_STATES.IN_INGREDIENTS) {
            if (!recipe.hasPreviousIngredient()) {
                speech.say('Das ist bereits die erste Zutat');
            } else {
                recipe.previousIngredient();
                speech.say(recipe.getCurrentIngredient());
            }
            // Recipe => instructions
        } else if (Helpers.getResultState(request) === CONSTANTS.RESULT_STATES.IN_INSTRUCTIONS) {
            if (!recipe.hasPreviousInstruction()) {
                Helpers.setResultState(request, CONSTANTS.RESULT_STATES.IN_INGREDIENTS);
                recipe.lastIngredient();
                recipe.resetInstructions();
                speech
                    .say('Okay, wieder zurück zu den Zutaten:')
                    .say(recipe.getCurrentIngredient());
            } else {
                recipe.previousInstruction();
                speech.say(recipe.getCurrentInstruction());
            }
        }

        Helpers.setRecipe(request, recipe);
        response.say(speech.ssml(true)).shouldEndSession(false);
    } else {
        Helpers.cannotBeUsedHereResponse(response);
    }
};
let handleRepeatSingleIntent = function(request, response) {
    console.log('' + arguments.callee.name);
    if (Helpers.getAppState(request) === CONSTANTS.APP_STATES.RESULT) {
        let recipe = Helpers.getRecipe(request);
        let speech = new AmazonSpeech();

        if (Helpers.getResultState(request) === CONSTANTS.RESULT_STATES.IN_INGREDIENTS) {
            speech
                .say(recipe.getCurrentIngredient());
        } else if (Helpers.getResultState(request) === CONSTANTS.RESULT_STATES.IN_INSTRUCTIONS) {
            speech
                .say(recipe.getCurrentInstruction());
        }

        response.say(speech.ssml(true)).shouldEndSession(false);
    } else {
        Helpers.cannotBeUsedHereResponse(response);
    }
};
let handleRepeatAllIntent = function(request, response) {
    console.log('' + arguments.callee.name);
    if (Helpers.getAppState(request) === CONSTANTS.APP_STATES.RESULT) {
        let recipe = Helpers.getRecipe(request);
        let speech = new AmazonSpeech();

        if (Helpers.getResultState(request) === CONSTANTS.RESULT_STATES.IN_INGREDIENTS) {
            recipe.resetIngredients();
            recipe.resetInstructions();
            speech
                .say('Okay, ich wiederhole nochmal alle Zutaten. Für ' + recipe.name + ' brauchst du:')
                .say(recipe.getCurrentIngredient());
        } else if (Helpers.getResultState(request) === CONSTANTS.RESULT_STATES.IN_INSTRUCTIONS) {
            recipe.resetInstructions();
            speech
                .say('Okay, ich wiederhole nochmal alle Arbeitsschritte. Um ' + recipe.name + ' zuzubereiten musst du:')
                .say(recipe.getCurrentInstruction());
        }

        Helpers.setRecipe(request, recipe);
        response.say(speech.ssml(true)).shouldEndSession(false);
    } else {
        Helpers.cannotBeUsedHereResponse(response);
    }
};


let handleInvokeNameIntent = function(request, response) {
    console.log('' + arguments.callee.name);
    // Menu => name
    if (Helpers.getAppState(request) === CONSTANTS.APP_STATES.MENU) {
        Helpers.setAppState(request, CONSTANTS.APP_STATES.SEARCH_NAME);
        let speech = new AmazonSpeech();
        speech.say('Welchen Namen hat das Rezept, das du suchst?');
        response.say(speech.ssml(true)).shouldEndSession(false);
    } else {
        Helpers.cannotBeUsedHereResponse(response);
    }
};
let handleInvokeCategoryIntent = function(request, response) {
    console.log('' + arguments.callee.name);
    // Menu => category
    if (Helpers.getAppState(request) === CONSTANTS.APP_STATES.MENU) {
        Helpers.setAppState(request, CONSTANTS.APP_STATES.SEARCH_CATEGORY);
        let speech = new AmazonSpeech();
        speech.say('In welcher Kategorie ist das Rezept, das du suchst?');
        response.say(speech.ssml(true)).shouldEndSession(false);
    } else {
        Helpers.cannotBeUsedHereResponse(response);
    }
};
let handleInvokeIngredientIntent = function(request, response) {
    console.log('' + arguments.callee.name);
    if (Helpers.getAppState(request) === CONSTANTS.APP_STATES.MENU) {
        Helpers.setAppState(request, CONSTANTS.APP_STATES.SEARCH_INGREDIENT);
        let speech = new AmazonSpeech();
        speech.say('Welche Zutaten beinhaltet das Rezept, das du suchst?');
        response.say(speech.ssml(true)).shouldEndSession(false);
    } else {
        Helpers.cannotBeUsedHereResponse(response);
    }
};
let handleInvokeMyRecipesIntent = function(request, response) {
    console.log('' + arguments.callee.name);
    if (Helpers.getAppState(request) === CONSTANTS.APP_STATES.MENU) {
        Helpers.setAppState(request, CONSTANTS.APP_STATES.SEARCH_MY_RECIPIES);
        let speech = new AmazonSpeech();
        speech.say('Wie heißt das Rezept, das du suchst?');
        response.say(speech.ssml(true)).shouldEndSession(false);
    } else {
        Helpers.cannotBeUsedHereResponse(response);
    }
};

let handleSearchByCategoryIntent = function(request, response) {
    console.log('' + arguments.callee.name);
    if (Helpers.getAppState(request) === CONSTANTS.APP_STATES.SEARCH_CATEGORY) {
        let speech = new AmazonSpeech();
        let categoryName = Helpers.getSlotValueOrNull(request, 'CATEGORY');
        if (categoryName != null) {
            return API.FoodAPI.getRecipeByCategory(categoryName)
                .then((responseRecipe) => {
                    if (responseRecipe != null) {
                        Helpers.setSearchString(request, categoryName);
                        Helpers.setRecipe(request, responseRecipe);
                        response
                            .say('Möchtest du ' + responseRecipe.getName() + ' zusammen mit mir kochen?')
                            .shouldEndSession(false);
                    } else {
                        response
                            .say('Ich konnte kein Rezept finden. versuche es erneut.')
                            .shouldEndSession(false);
                    }
                });
        } else {
            speech.say('Diese Kategorie habe ich nicht gefunden. Versuche es erneut');
        }
        response
            .say(speech.ssml(true))
            .shouldEndSession(false);
    } else {
        Helpers.cannotBeUsedHereResponse(response);
    }
};
let handleSearchByNameIntent = function (request, response) {
    console.log('' + arguments.callee.name);
    // recipe by name => name
    if (Helpers.getAppState(request) === CONSTANTS.APP_STATES.SEARCH_NAME) {
        let speech = new AmazonSpeech();
        let recipeName = Helpers.getSlotValueOrNull(request, 'NAME');
        if (recipeName != null) {
            return API.FoodAPI.getRecipeByName(recipeName)
                .then((responseRecipe) => {
                    if (responseRecipe != null) {
                        Helpers.setSearchString(request, recipeName);
                        Helpers.setRecipe(request, responseRecipe);
                        response
                            .say('Möchtest du ' + responseRecipe.getName() + ' zusammen mit mir kochen?')
                            .shouldEndSession(false);
                    } else {
                        response
                            .say('Ich konnte kein Rezept finden. versuche es erneut.')
                            .shouldEndSession(false);
                    }
                });
        } else {
            speech.say('Den Namen des Gerichts konnte ich leider nicht verstehen. Versuche es erneut');
        }
        response
            .say(speech.ssml(true))
            .shouldEndSession(false);

    }  else {
        Helpers.cannotBeUsedHereResponse(response);
    }
};
let handleSearchMyRecipesByNameIntent = function (request, response) {
    console.log('' + arguments.callee.name);

    //    my recipes => name
    if (Helpers.getAppState(request) === CONSTANTS.APP_STATES.SEARCH_MY_RECIPIES) {
        let speech = new AmazonSpeech();
        let recipeName = Helpers.getSlotValueOrNull(request, 'NAME');
        if (recipeName != null) {
            let dbAPI = new DB.DatabaseAPI();
            let recipes = dbAPI.getRecipesByName(recipeName);
            if ((recipes != null) && (recipes[0] != null)) {
                let recipe = recipes[0];
                Helpers.setRecipe(request, recipe);
                Helpers.setAppState(request, CONSTANTS.APP_STATES.RESULT);
                Helpers.setResultState(request, CONSTANTS.RESULT_STATES.IN_INGREDIENTS);
                Helpers.resetSearchString(request);

                speech.say('Okay, lass uns ' + recipe.name + ' zusammen kochen.').pause('200ms')
                    .say('Für ' + recipe.name + ' brauchst du:' + recipe.getCurrentIngredient());

                Helpers.setRecipe(request, recipe);
            } else {
                speech.say('Leider konnte ich kein Rezept mit diesem Namen finden. Versuche es erneut.');
            }
        } else {
            speech.say('Leider konnte ich dich nicht verstehen. Versuche es erneut.');
        }
        response
            .say(speech.ssml(true))
            .shouldEndSession(false);
    } else {
        Helpers.cannotBeUsedHereResponse(response);
    }
};
let handleSearchByIngredientIntent = function (request, response) {
    console.log('' + arguments.callee.name);
    if (Helpers.getAppState(request) === CONSTANTS.APP_STATES.SEARCH_INGREDIENT) {
        let speech = new AmazonSpeech();
        let ingredientNames = Helpers.getSlotValueOrNull(request, 'INGREDIENT');

        if (ingredientNames != null) {

            // Remove filling-words
            ingredientNames = _.replace(ingredientNames, 'und', ' ');
            ingredientNames = _.replace(ingredientNames, 'oder', ' ');
            ingredientNames = _.replace(ingredientNames, 'außerdem', ' ');
            ingredientNames = _.replace(ingredientNames, 'sowie', ' ');

            return API.FoodAPI.getRecipeByIngredient(ingredientNames)
                .then((responseRecipe) => {
                    if (responseRecipe != null) {
                        Helpers.setSearchString(request, ingredientNames);
                        Helpers.setRecipe(request, responseRecipe);
                        response
                            .say('Möchtest du ' + responseRecipe.getName() + ' zusammen mit mir kochen?')
                            .shouldEndSession(false);
                    } else {
                        response
                            .say('Für diese Zutaten habe ich kein Rezept gefunden. Versuche es erneut.')
                            .shouldEndSession(false)
                    }
                });
        } else {
            speech.say('Die Zutaten konnte ich leider nicht verstehen. Versuche es erneut');
        }
        response
            .say(speech.ssml(true))
            .shouldEndSession(false);
    } else {
        Helpers.cannotBeUsedHereResponse(response);
    }

};

/*
 * Built-in intents
 */
let handleHelpIntent = function(request, response) {
    console.log('' + arguments.callee.name);
    let speech = new AmazonSpeech();

    // During navigating through recipes
    if (Helpers.getAppState(request) === CONSTANTS.APP_STATES.RESULT) {
        let appendix1 = '';
        let appendix2 = '';
        if (Helpers.getAppState(request) === CONSTANTS.RESULT_STATES.IN_INGREDIENTS) {
            appendix1 = 'die Zutat';
            appendix2 = 'alle Zutaten';
        } else {
            appendix1 = 'den Schritt';
            appendix2 = 'alle Schritte ';
        }
        speech.say('Navigiere mit ')
            .emphasis('moderate', 'weiter')
            .say(',')
            .emphasis('moderate', 'zurück')
            .say(',')
            .emphasis('moderate', 'wiederhole ' + appendix1)
            .say('oder')
            .emphasis('moderate', 'wiederhole ' + appendix2)
            .say('durch\'s Rezept.');
    //    In main menu
    } else if (Helpers.getAppState(request) === CONSTANTS.APP_STATES.MENU) {
        speech
            .say('Hier bist du im Menü.')
            .pause('200ms')
            .say('Sage ')
            .emphasis('moderate', 'nach kategorie suchen').say(', um Rezepte nach ihren Kategorien zu suchen,')
            .pause('200ms')
            .emphasis('moderate', 'nach namen suchen').say(', um Rezepte anhand ihres namens zu finden oder')
            .pause('200ms')
            .emphasis('moderate', 'nach zutaten suchen').say(', um Rezepte anhand ihrer Zutaten zu finden.')
            .pause('200ms')
            .emphasis('moderate', 'Nach meinen rezepten suchen').say('lässt dich nach gespeicherten Rezepten suchen.');
    //
    } else if (Helpers.getAppState(request) === CONSTANTS.APP_STATES.SEARCH_CATEGORY) {
        speech
            .say('Hier kannst du nach Rezepten einer Kategorie suchen.')
            .pause('200ms')
            .say('Es gibt die Kategorien: ').emphasis('moderate', _.join(CONSTANTS.AVAILABLE_CATEGORIES_ARRAY, ', ')).say('.')
            .pause('200ms')
            .say('Um in einer Kategorie zu suchen, sage beispielsweise: ').emphasis('moderate', 'durchsuche die kategorie fisch');
    } else if (Helpers.getAppState(request) === CONSTANTS.APP_STATES.SEARCH_NAME) {
        speech
            .say('Hier findest du Rezepte anhand ihrer Namen.')
            .pause('200ms')
            .say('Um ein Rezept zu finden, sage beispielsweise: ').emphasis('moderate', 'durchsuche alle rezepte nach ratatouille');
    } else  if (Helpers.getAppState(request) === CONSTANTS.APP_STATES.SEARCH_INGREDIENT) {
        speech
            .say('Hier kannst du nach Rezepten suchen, die bestimmte Zutaten beinhalten.')
            .pause('200ms')
            .say('Um nach einem Rezept mit deinen Zutaten zu suchen, sage beispielsweise: ').emphasis('moderate', 'durchsuche die rezepte nach den zutaten tomate gurke und zwiebel').say('.');
    } else  if (Helpers.getAppState(request) === CONSTANTS.APP_STATES.SEARCH_MY_RECIPIES) {
        speech
            .say('Hier kannst du nach deinen gespeicherten Rezepten suchen.')
            .pause('200ms')
            .say('Um nach einem gespeicherten Rezept zu suchen, sage beispielsweise: ').emphasis('moderate', 'durchsuche meine rezepte nach spaghetti bolognese').say('.');
    }
    speech
        .pause('500ms')
        .say('Du kannst Rezepte jederzeit mit')
        .emphasis('moderate', 'kochbuch rezept abbrechen')
        .pause('100ms')
        .say('schließen und zum Hauptmenü zurückkehren.')
        .emphasis('moderate', 'kochbuch beenden')
        .say('schließt den Skill.');
    let reprompt = "Was würdest du gerne machen?";
    response
        .say(speech.ssml(true))
        .reprompt(reprompt)
        .shouldEndSession(false);
};
let handleStopIntent = function(request, response) {
    console.log('' + arguments.callee.name);
    let stopOutput = 'Einen guten Appetit und bis bald';
    response
        .say(stopOutput)
        .shouldEndSession(true);
};
let handleCancelIntent = function(request, response) {
    console.log('' + arguments.callee.name);
    Helpers.setAppState(request, CONSTANTS.APP_STATES.MENU);
    Helpers.resetRecipe(request);
    Helpers.resetResultState(request);
    Helpers.resetSearchString(request);
    let speech = new AmazonSpeech()
        .say('Okay, du bist jetzt im Menü. hier kannst du nach')
        .emphasis('moderate', 'Namen, Zutaten, Kategorien oder deinen gespeicherten Rezepten')
        .say('suchen. Nach was würdest du gerne suchen?');
    response
        .say(speech.ssml(true))
        .shouldEndSession(false);
};

let handleError = function (exception, request, response) {
    console.log('' + arguments.callee.name);
    response
        .say('Ups, da ist was schiefgelaufen.')
        .shouldEndSession(true);
    console.error(exception.message);
};

let handleSessionEnded = function (request, response) {
    console.log('' + arguments.callee.name);

};

module.exports = {handleYesIntent, handleNoIntent, handleHelpIntent, handleStopIntent, handleCancelIntent, handleNextIntent,
    handleRepeatSingleIntent, handleRepeatAllIntent, handlePreviousIntent, handleInvokeNameIntent, handleInvokeCategoryIntent,
    handleInvokeMyRecipesIntent, handleInvokeIngredientIntent, handleSearchByCategoryIntent, handleSearchByNameIntent,
    handleSearchMyRecipesByNameIntent, handleSearchByIngredientIntent, handleError, handleSessionEnded};