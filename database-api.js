let assert = require('assert');
let _ = require('lodash');
let JsonDB = require('node-json-db');

let CONSTANTS = require('./constants');
let Models = require('./models');



class DatabaseAPI {

    constructor() {
        this.dataPath = '/recipes';
        this.DB = new JsonDB('mein-kochbuch', false, true);
    }

    reset() {
        this.DB.delete(this.dataPath);
    }

    saveRecipe(recipe) {
        assert(recipe != null, 'Recipe is empty');
        assert(recipe instanceof Models.Recipe, 'Recipe not of type Recipe. ' + typeof recipe + ' given');

        let oldRecipes;
        try {
            oldRecipes = this.DB.getData(this.dataPath);
        } catch(error) {
            if (error.name === 'DataError') {
                console.log('DB not found. Create it!');
            } else {
                console.error(error);
            }
            oldRecipes = [];
        }

        // Find the index of a recipe with the same title
        let oldIndex = _.findIndex(oldRecipes, {name: recipe.getName()});
        if (oldIndex === -1) {
            oldRecipes.push(recipe.toJSON());
            this.DB.push(this.dataPath, oldRecipes);
            this.DB.save();

            return true;
        }
        console.log('Recipe with same name is already saved!', recipe.getName());
        return false;
    }

    getRecipesByName(name) {
        assert(typeof name === 'string', 'Provided name is no string');
        let recipesToReturn;
        try {
            recipesToReturn = this.DB.getData(this.dataPath);
            recipesToReturn = _.filter(recipesToReturn, (recipe) => (new RegExp(name, 'i')).test(recipe.name));
        } catch (error) {
            recipesToReturn = [];
        }
        recipesToReturn = _.map(recipesToReturn, (recipe) => new Models.Recipe(recipe.name, recipe.category, recipe.ingredients, recipe.instructions, recipe.ingredientIndex, recipe.instructionIndex));

        return recipesToReturn;
    }

    getRecipesByCategory(category) {
        assert(typeof category === 'string', 'Provided category is no string');
        assert(CONSTANTS.CATEGORY_SLOTS_VALUES_ARRAY.includes(category), 'Category name is unknown: ' + category);
        let recipesToReturn;
        try {
            recipesToReturn = this.DB.getData(this.dataPath);
            recipesToReturn = _.filter(recipesToReturn, (recipe) => (new RegExp(category, 'i')).test(recipe.category));
        } catch (error) {
            recipesToReturn = [];
        }
        return recipesToReturn;
    }

    findRecipe(name) {
        assert(typeof name === 'string', 'Provided name is no string');
        let recipeToReturn;
        try {
            recipeToReturn = this.DB.getData(this.dataPath);
            recipeToReturn = _.filter(recipeToReturn, (recipe) => (recipe.name.toLowerCase() === name.toLowerCase()));
        } catch (error) {
            recipeToReturn = null;
        }
        return (recipeToReturn != null) && (recipeToReturn.length > 0);
    }

}

function runTest() {
    let DBApi = new DatabaseAPI();

    let recipe = new Models.Recipe('Yolo1', 'starter', ['1', '2', '3'], ['1', '2', '3']);
    let recipe1 = new Models.Recipe('Yolo2', 'starter', ['1', '2', '3'], ['1', '2', '3']);
    let recipe2 = new Models.Recipe('Yolo1', 'starter', ['1', '2', '3'], ['1', '2', '3']);
    let recipe3 = new Models.Recipe('Spaghetti Bolognese', 'starter', ['1', '2', '3'], ['1', '2', '3']);
    let recipe4 = new Models.Recipe('Fleischbällchen mit Spaghetti', 'starter', ['1', '2', '3'], ['1', '2', '3']);

    DBApi.saveRecipe(recipe);
    DBApi.saveRecipe(recipe1);
    DBApi.saveRecipe(recipe2);
    DBApi.saveRecipe(recipe3);
    DBApi.saveRecipe(recipe4);
//    #############

    console.log(DBApi.getRecipesByName('spaghetti'));
    console.log(DBApi.getRecipesByName('Spaghetti'));
    console.log(DBApi.getRecipesByName('spaghetti bolognese'));
    console.log(DBApi.getRecipesByName('bolognese'));

//    #############

    console.log('find spaghetti', DBApi.findRecipe('spaghetti'));
    console.log('find spaghetti bolognese', DBApi.findRecipe('spaghetti bolognese'));
    console.log('find Spaghetti Bolognese', DBApi.findRecipe('Spaghetti Bolognese'));
    console.log('find yolo1', DBApi.findRecipe('yolo1'));
    console.log('find yolo', DBApi.findRecipe('yolo'));

}

function seedDB() {
    let DBApi = new DatabaseAPI();

    DBApi.reset();

    let ingredients = ['500 Gramm Mehl', '20 bis 40 Gramm Hefe', '10 Gramm Salz', '10 ml Olivenöl', '1 Prise Zucker', '200 bis 250 ml lauwarmes Wasser'];
    let instructions = ['Mehl in eine Schüssel geben, mit der Hand eine Kuhle hineindrücken.', 'Die Hefe zusammen mit dem Zucker und 200 ml Wasser in einem Gefäß auflösen.',
        'Die aufgelöste Hefe, das Olivenöl und das Salz in die Kuhle geben.', 'Alle Zutaten nun mit den Händen zu einem glatten Teig verkneten, bei Bedarf etwas Wasser nachgeben.',
        'Der Teig muss nun ca. 1 Stunde an einem warmen Ort ruhen, dann kann er verwendet werden.'];
    let recipe = new Models.Recipe('Pizzateig', 'miscellaneous', ingredients, instructions);
    DBApi.saveRecipe(recipe);

    let ingr = ['Zutat 1', 'Zutat 2'];
    let instr = ['Schritt 1', 'Schritt 2'];
    let rec = new Models.Recipe('Test', 'side', ingr, instr);
    DBApi.saveRecipe(rec);
}

// runTest();
// seedDB();

module.exports = {DatabaseAPI};