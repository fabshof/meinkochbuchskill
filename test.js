let express = require("express");
let alexa = require("alexa-app");
let AmazonSpeech = require('ssml-builder/amazon_speech');

let PORT = process.env.PORT || 8080;




let app = express();
let alexaApp = new alexa.app("test");
alexaApp.invocationName = 'test invocation';

alexaApp.express({
    expressApp: app,
    checkCert: false,
    debug: true
});

app.set("view engine", "ejs");

alexaApp.customSlot('ExampleSlot', [
    {
        value: 'Example',
        id: 'ExampleID',
        synonyms: ['Example1', 'Example2']
    }
]);

alexaApp.launch(function(request, response) {
    // Do something
});

alexaApp.intent("example_intent", {
        "slots": {'EXAMPLE': 'ExampleSlot'},
        "utterances": [
            '{ich möchte|} {-|EXAMPLE} durchsuchen',
            '{ich möchte|ich mag} nach {-|EXAMPLE} suchen',
        ]
    },
    function (request, response) {
        let example = request.slot('EXAMPLE', 'default');

        let speech = new AmazonSpeech();
        speech.say('Hallo')
            .pause('200ms')
            .say('Du suchst wohl nach ' + example + '?');
        response
            .say(speech.ssml(true))
            .shouldEndSession(false);
    }
);



app.listen(PORT);













