let _ = require('lodash');
let assert = require('assert');
let request = require('request-promise');

let Models = require('./models');

const FOOD_BASE_URL = 'https://www.themealdb.com/api/json/v1/1';

class FoodAPI {

    static responseToRecipe(json) {

        if (json != null) {

            let numberPattern = /\d+/g;
            if (json.hasOwnProperty('meals')) {


                // Get a random recipe from the set of recipes
                let meals = json['meals'];
                assert(meals != null, 'field meals not available');

                if (meals.length === 0) {
                    return null;
                }
                assert(meals.length > 0, 'no meals available');

                let meal = meals[[Math.floor(Math.random() * meals.length)]];

                let name = meal['strMeal'];
                let instructions = meal['strInstructions'].split('\r\n');
                let category = meal['strCategory'].toLowerCase();
                let ingredients = [];

                let fittingKeys = _.filter(_.keys(meal), (key) => /Ingredient/.test(key));
                _.each(fittingKeys, (value, key) => {
                    let number = value.match(numberPattern)[0];
                    let ingredientName = meal[value];
                    if (ingredientName) {
                        let ingredientMeasure = meal['strMeasure' + number];
                        ingredients.push(((ingredientMeasure && ingredientMeasure == 1) ? '' : ingredientMeasure) + ' ' + ingredientName)
                    }
                });

                return new Models.Recipe(name, category, ingredients, instructions);
            }
        }
        return null;
    }

    static getRandomRecipe() {
        let options = {
            uri: FOOD_BASE_URL + '/random.php',
            json: true
        };
        return request(options)
            .then((data) => {
                return this.responseToRecipe(data);
            });
    }

    static getRecipeByCategory(categoryName) {
        assert(typeof categoryName === 'string', 'categoryName is no string');
        assert(categoryName !== '', 'categoryName is empty');

        let options = {
            uri: FOOD_BASE_URL + '/filter.php?c=' + categoryName,
            json: true
        };
        return request(options)
            .then((data) => {
                if (data.hasOwnProperty('meals')) {
                    let meals = data['meals'];
                    if (meals.length > 0) {
                        let id = (meals[Math.floor(meals.length * Math.random())])['idMeal'];
                        let recipeOptions = {
                            uri: FOOD_BASE_URL + '/lookup.php?i=' + id,
                            json: true
                        };
                        return request(recipeOptions);
                    }
                }
                return null;
            }).then((categoryData) => {
                return this.responseToRecipe(categoryData);
            });
    }

    static getRecipeByIngredient(ingredientName) {
        assert(typeof ingredientName === 'string', 'ingredientName is no string');
        assert(ingredientName !== '', 'ingredientName is empty');
        ingredientName = _.replace(ingredientName, / +(?= )/g, '%20');
        let options = {
            uri: FOOD_BASE_URL + '/filter.php?i=' + ingredientName,
            json: true
        };
        return request(options)
            .then((data) => {
                if (data.hasOwnProperty('meals')) {
                    let meals = data['meals'];
                    if (meals.length > 0) {
                        let id = (meals[Math.floor(meals.length * Math.random())])['idMeal'];
                        let recipeOptions = {
                            uri: FOOD_BASE_URL + '/lookup.php?i=' + id,
                            json: true
                        };
                        return request(recipeOptions);
                    }
                }
                return null;
            })
            .then((ingredientData) => {
                return this.responseToRecipe(ingredientData);
            });
    }

    static getRecipeByName(name) {
        name = name.replace(' ', '%20');
        let options = {
            uri: FOOD_BASE_URL + '/search.php?s=' + name,
            json: true
        };
        return request(options)
            .then((data) => {
                return this.responseToRecipe(data);
            });
    }


}

function runTest() {
    FoodAPI.getRandomRecipe()
        .then((recipe) => {
            console.log('Recipe:', recipe);
        });
}

// runTest();


module.exports = {FoodAPI};
